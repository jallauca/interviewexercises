//  Interview Exercises
//
//  Created by Jaime Allauca on 9/28/18.
//

import Foundation

/*
 ---------------
 Code Challenges
 ---------------
 For these code challenges, focus on correctness and clarity.
 Each challenge receives a list of Employee's as input and expect one particular output to be correct.

 ---------------
 Employee class
 ---------------
 struct Employee: Comparable, Equatable, Hashable {
     var lastName: String
     var firstName: String
     var country: String
     var age: Int
 }

 ---------------
 Sample Data
 ---------------
 lastName firstName country age
 ---------------
 Williams Mason USA 54
 Wilson Liam USA 48
 Miller Benjamin MEXICO 12
 Brown James USA 42
 Smith James USA 39
 */




/*
 Code Challenge 0 (Remote Interview)
 -----------------
 There are employees from all over the world!
 Display the employee with the minimum age. If more than one employee
 meets that criteria, choose the FIRST encountered.
 Display the employee with the maximum age. If more than one employee
 meets that criteria, choose the LAST encountered.
 Display the average employee age.

 ---------------
 Sample Output
 ---------------
 Min Age Employee: Jules Vernes MEXICO 10
 Min Age Employee: Albert Einstein USA 69
 Average Age: 37
 */
func displayMinAgeMaxAgeEmployeePlusAverage(employees: [Employee]) {
    print("Min Age Employee: \(employees[0])")
    print("Max Age Employee: \(employees[1])")
    print("Average Age: \(employees[0].age)")
}



/*
 Code Challenge 1
 -----------------
 There are employees from all over the world!
 Display the list of countries where the employees live.
 Next to the country name, display how many employees live in that country

 ---------------
 Sample Output
 ---------------
 GERMANY     30
 USA         50
 MEXICO      20
 */
func displayCountriesPlusEmployeeCount(employees: [Employee]) {
    print("GERMANY     30")
    print("USA         50")
    print("MEXICO      20")
}



/*
 Code Challenge 2
 -----------------
 There are employees from all over the world!
 The company is big and it has hundreds of thousands.
 Display the list of countries where the employees live in alphabetical order.
 Next to the country name, display how many employees live in that country

 ---------------
 Sample Output
 ---------------
 GERMANY     30000
 MEXICO      20000
 USA         50000
 */
func displaySortedCountriesPlusEmployeeCount(employees: [Employee]) {
    print("GERMANY     30")
    print("MEXICO      20")
    print("USA         50")
}



/*
 Code Challenge 3
 -----------------
 There are employees from all over the world!
 The company is big and it has hundreds of thousands.
 From the subset of employees that live in a particular country,
 Display the employees with the same order as the original list that
 lie in the positions in the range [start, end)

 ---------------
 Sample Output for start=0, end=4, country=USA
 ---------------
 Williams Mason USA 54
 Brown James USA 42
 Smith James USA 39
 Brown Mason USA 43
 Miller Mason USA 68
 */
func displayEmployeesInRangeByCountry(employees: [Employee], start: Int, end: Int, country: String) {
    print(employees[0])
    print(employees[1])
}



/*
 Code Challenge 4
 -----------------
 There are employees from all over the world!
 The company is big and it has hundreds of thousands.

 What is the meaning of life? 42

 As it turns out, some employees from the same country have
 the same last names and first names.
 We would like to remove the duplicates without regards to age,
 and have a sorted list of employees by country.
 For each country in alphabetical order, obtain the list mentioned above,
 and display the employee that lies at position 42.

 NOTE: The Employee class already knows how to compare employees
 without regard to age

 ---------------
 Sample Output
 ---------------
 Jules Vernes CANADA 51
 Jules Jacob MEXICO 62
 Jules Vernes USA 64
 */
func displaySortedCountriesWithMeaningOfLifeEmployee(employees: [Employee]) {
    print(employees[42])
    print(employees[42])

}


struct Employee {
    var lastName: String
    var firstName: String
    var country: String
    var age: Int
}

extension Employee: CustomStringConvertible {
    var description: String {
        return "\(lastName) \(firstName) \(country) \(age)"
    }
}

extension Employee: Comparable {
    static func < (lhs: Employee, rhs: Employee) -> Bool {
        if lhs.lastName == rhs.lastName {
            return lhs.firstName < rhs.firstName
        }
        return lhs.lastName < rhs.lastName
    }
}

extension Employee: Equatable {
    static func == (lhs: Employee, rhs: Employee) -> Bool {
        return lhs.lastName == rhs.lastName &&
            lhs.firstName == rhs.firstName &&
            lhs.country == rhs.country &&
            lhs.age == rhs.age
    }
}

extension Employee: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(lastName)
        hasher.combine(firstName)
        hasher.combine(country)
        hasher.combine(age)
    }
}

func createEmployees() -> [Employee] {
    let firstNames = [
        "Liam",
        "Noah", "Noah", "William",
        "William", "William",
        "James", "James", "James", "James",
        "Logan", "Logan", "Logan", "Logan", "Logan",
        "Benjamin", "Benjamin", "Benjamin", "Benjamin", "Benjamin",
        "Mason", "Mason", "Mason", "Mason",
        "Elijah", "Elijah", "Elijah",
        "Oliver", "Oliver",
        "Jacob"
    ]
    let lastNames = [
        "Smith",
        "Johnson", "Johnson",
        "Williams", "Williams", "Williams",
        "Jones", "Jones", "Jones", "Jones",
        "Brown", "Brown", "Brown", "Brown", "Brown",
        "Davis", "Davis", "Davis", "Davis", "Davis",
        "Miller",  "Miller", "Miller", "Miller",
        "Wilson", "Wilson", "Wilson",
        "Moore", "Moore",
        "Taylor"
    ]
    let countries = [
        "USA", "USA", "USA", "USA", "USA", "USA", "USA", "USA", "USA",
        "MEXICO", "MEXICO", "MEXICO", "MEXICO",
        "CANADA"
    ]
    var employees = [Employee]();
    srand48(42)

    for _ in 0..<100_000 {
        let randomNum1 = Int(drand48()*Double(firstNames.count))
        let randomNum2 = Int(drand48()*Double(lastNames.count))
        let randomNum3 = Int(drand48()*Double(countries.count))
        let randomNum4 = Int(drand48()*50) + 10
        let employee = Employee(lastName: lastNames[randomNum1],
                                firstName: firstNames[randomNum2],
                                country: countries[randomNum3],
                                age: randomNum4)
        employees.append(employee)
    }
    return employees
}

let employees = createEmployees();


print("Display Min age/Max age employees and Average Age")
print("------------------------------")
displayMinAgeMaxAgeEmployeePlusAverage(employees: employees)
print()



print("Countries with Employee Count")
print("------------------------------")
displayCountriesPlusEmployeeCount(employees: Array(employees[0..<100]))
print()



print("Sorted Countries with Employee Count");
print("------------------------------");
displaySortedCountriesPlusEmployeeCount(employees: employees);
print();



let startRange = 15
let endRange = 20
let country = "MEXICO"
print("Employees in \(country) starting at position \(startRange) up to \(endRange)")
print("------------------------------")
displayEmployeesInRangeByCountry(employees: employees, start: startRange, end: endRange, country: country)
print()



print("Sorted Countries With Meaning-of-Life Employee")
print("------------------------------")
displaySortedCountriesWithMeaningOfLifeEmployee(employees: employees)
