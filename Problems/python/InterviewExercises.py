import random

class InterviewExercises:

    '''
        ---------------
        Code Challenges
        ---------------
        For these code challenges, focus on correctness and clarity.
        Each challenge receives a list of Employee's as input and expect one particular output to be correct.

        ---------------
        Employee class
        ---------------
        class Employee:
            def __init__(self, lastName, firstName, country, age):
                self.lastName = lastName
                self.firstName = firstName
                self.country = country
                self.age = age

        ---------------
        Sample Data
        ---------------
        lastName firstName country age
        ---------------
        Williams Mason USA 54
        Wilson Liam USA 48
        Miller Benjamin MEXICO 12
        Brown James USA 42      
        Smith James USA 39
    '''



    '''
        Code Challenge 0 (Remote Interview)
        -----------------
        There are employees from all over the world!
        Display the employee with the minimum age. If more than one employee
        meets that criteria, choose the FIRST encountered.
        Display the employee with the maximum age. If more than one employee
        meets that criteria, choose the LAST encountered.
        Display the average employee age.

        ---------------
        Sample Output
        ---------------
        Min Age Employee: Jules Vernes MEXICO 10
        Min Age Employee: Albert Einstein USA 69
        Average Age: 37
    '''
    def displayMinAgeMaxAgeEmployeePlusAverage(self, employees):
        print("Min Age Employee: ", employees[0])
        print("Min Age Employee: ", employees[1])
        print("Average Age: ", employees[0].age)



    '''
        Code Challenge 1
        -----------------
        There are employees from all over the world!
        Display the list of countries where the employees live.
        Next to the country name, display how many employees live in that country

        ---------------
        Sample Output
        ---------------
        GERMANY     30
        USA         50
        MEXICO      20
    '''
    def displayCountriesPlusEmployeeCount(self, employees):
        print("GERMANY     30")
        print("USA         50")
        print("MEXICO      20")



    '''
        Code Challenge 2
        -----------------
        There are employees from all over the world!
        The company is big and it has hundreds of thousands.
        Display the list of countries where the employees live in alphabetical order.
        Next to the country name, display how many employees live in that country

        ---------------
        Sample Output
        ---------------
        GERMANY     30000
        MEXICO      20000
        USA         50000
    '''
    def displaySortedCountriesPlusEmployeeCount(self, employees):
        print("GERMANY     30000")
        print("MEXICO      20000")
        print("USA         50000")



    '''
        Code Challenge 3
        -----------------
        There are employees from all over the world!
        The company is big and it has hundreds of thousands.
        From the subset of employees that live in a particular country,
        Display the employees with the same order as the original list that 
        lie in the positions in the range [start, end)

        ---------------
        Sample Output for start=0, end=4, country=USA
        ---------------
        Williams Mason USA 54
        Brown James USA 42
        Smith James USA 39
        Brown Mason USA 43
        Miller Mason USA 68
    '''
    def displayEmployeesInRangeByCountry(self, employees, start, end, country):
        print(employees[0])
        print(employees[1])



    '''
        Code Challenge 4
        -----------------
        There are employees from all over the world!
        The company is big and it has hundreds of thousands.

        What is the meaning of life? 42

        As it turns out, some employees from the same country have
        the same last names and first names.
        We would like to remove the duplicates without regards to age,
        and have a sorted list of employees by country.
        For each country in alphabetical order, obtain the list mentioned above,
        and display the employee that lies at position 42.

        NOTE: The Employee class already knows how to compare employees
        without regard to age

        ---------------
        Sample Output
        ---------------
        Jules Vernes CANADA 51
        Jules Jacob MEXICO 62
        Jules Vernes USA 64
    '''
    def displaySortedCountriesWithMeaningOfLifeEmployee(self, employees):
        print(employees[42])
        print(employees[42])



    def main(self):
        employees = self.createEmployees()


        print("Display Min age/Max age employees and Average Age")
        print("------------------------------")
        self.displayMinAgeMaxAgeEmployeePlusAverage(employees)
        print("")



        print("Countries with Employee Count")
        print("------------------------------")
        self.displayCountriesPlusEmployeeCount(employees[0:100])
        print("")



        print("Sorted Countries with Employee Count")
        print("------------------------------")
        self.displaySortedCountriesPlusEmployeeCount(employees)
        print("")



        startRange = 15
        endRange = 20
        country = "MEXICO"
        print(f"Employees in {country} starting at position {startRange} up to {endRange}")
        print("------------------------------")
        self.displayEmployeesInRangeByCountry(employees, startRange, endRange, country)
        print("")



        print("Sorted Countries With Meaning-of-Life Employee")
        print("------------------------------")
        self.displaySortedCountriesWithMeaningOfLifeEmployee(employees)



    def createEmployees(self):
        firstNames = [
            "Liam", 
            "Noah", "Noah", "William",
            "William", "William",
            "James", "James", "James", "James",
            "Logan", "Logan", "Logan", "Logan", "Logan",
            "Benjamin", "Benjamin", "Benjamin", "Benjamin", "Benjamin",
            "Mason", "Mason", "Mason", "Mason",
            "Elijah", "Elijah", "Elijah",
            "Oliver", "Oliver",
            "Jacob"
        ]
        lastNames = [
            "Smith",
            "Johnson", "Johnson",
            "Williams", "Williams", "Williams",
            "Jones", "Jones", "Jones", "Jones",
            "Brown", "Brown", "Brown", "Brown", "Brown", 
            "Davis", "Davis", "Davis", "Davis", "Davis",
            "Miller",  "Miller", "Miller", "Miller", 
            "Wilson", "Wilson", "Wilson", 
            "Moore", "Moore",
            "Taylor" 
        ]
        countries = [
            "USA", "USA", "USA", "USA", "USA", "USA", "USA", "USA", "USA",
            "MEXICO", "MEXICO", "MEXICO", "MEXICO",
            "CANADA"
        ]
        employees = []
        random.seed(42)
        for i in range(100000):
            randomNum1 = random.randint(0, len(lastNames) - 1)
            randomNum2 = random.randint(0, len(firstNames) - 1)
            randomNum3 = random.randint(0, len(countries) - 1)
            randomNum4 = random.randint(10, 60)
            employee = Employee(
                lastNames[randomNum1],
                firstNames[randomNum2],
                countries[randomNum3],
                randomNum4
            )
            employees.append(employee)
        return employees


class Employee:

    def __init__(self, lastName, firstName, country, age):
        self.lastName = lastName
        self.firstName = firstName
        self.country = country
        self.age = age

    def __str__(self):
        return self.lastName + " " + self.firstName + " " + self.country + " " + str(self.age)

    def __hash__(self):
        return hash((self.lastName, self.firstName, self.country, self.age))

    def __eq__(self, other):
        if not isinstance(other, Employee):
            return False
            
        return self.firstName == other.firstName and \
               self.lastName == other.lastName and \
               self.country == other.country and \
               self.age == other.age

    def __ne__(self, other):
        return not (self == other)
    
    def __lt__(self, other):
        if self.lastName == other.lastName:
            return self.firstName < other.firstName
        return self.lastName < other.lastName


myapp = InterviewExercises()
myapp.main()