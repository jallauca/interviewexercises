﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace InterviewExercises
{
    class InterviewExercises
    {

        /* 
            ---------------
            Code Challenges
            ---------------
            For these code challenges, focus on correctness and clarity.
            Each challenge receives a list of Employee's as input and expect one particular output to be correct.

            ---------------
            Employee class
            ---------------
            struct Employee: IComparable<Employee> {
                public String lastName;
                public String firstName;
                public String country;
                public int age;
            }

            ---------------
            Sample Data
            ---------------
            lastName firstName country age
            ---------------
            Williams Mason USA 54
            Wilson Liam USA 48
            Miller Benjamin MEXICO 12
            Brown James USA 42      
            Smith James USA 39
        */



        /* 
            ---------------
            Code Challenges
            ---------------
            For these code challenges, focus on correctness and clarity.
            Each challenge receives a list of Employee's as input and expect one particular output to be correct.

            ---------------
            Employee class
            ---------------
            class Employee implements Comparable<Employee> {
                String lastName;
                String firstName;
                String country;
                int age;
            }

            ---------------
            Sample Data
            ---------------
            lastName firstName country age
            ---------------
            Williams Mason USA 54
            Wilson Liam USA 48
            Miller Benjamin MEXICO 12
            Brown James USA 42      
            Smith James USA 39
        */



        /*
            Code Challenge 0 (Remote Interview)
            -----------------
            There are employees from all over the world!
            Display the employee with the minimum age. If more than one employee
            meets that criteria, choose the FIRST encountered.
            Display the employee with the maximum age. If more than one employee
            meets that criteria, choose the LAST encountered.
            Display the average employee age.

            ---------------
            Sample Output
            ---------------
            Min Age Employee: Jules Vernes MEXICO 10
            Min Age Employee: Albert Einstein USA 69
            Average Age: 37
        */
        public void DisplayMinAgeMaxAgeEmployeePlusAverage(List<Employee> employees)
        {
            Console.WriteLine($"Min Age Employee: {employees[0]}");
            Console.WriteLine($"Min Age Employee: {employees[1]}");
            Console.WriteLine($"Average Age: {employees[0].age}");
        }



        /*
            Code Challenge 1
            -----------------
            There are employees from all over the world!
            Display the list of countries where the employees live.
            Next to the country name, display how many employees live in that country

            ---------------
            Sample Output
            ---------------
            GERMANY     30
            USA         50
            MEXICO      20
            */
        public void DisplayCountriesPlusEmployeeCount(List<Employee> employees)
        {
            Console.WriteLine("GERMANY     30");
            Console.WriteLine("USA         50");
            Console.WriteLine("MEXICO      20");
        }




        /*
            Code Challenge 2
            -----------------
            There are employees from all over the world!
            The company is big and it has hundreds of thousands.
            Display the list of countries where the employees live in alphabetical order.
            Next to the country name, display how many employees live in that country

            ---------------
            Sample Output
            ---------------
            GERMANY     30000
            MEXICO      20000
            USA         50000
            */
        public void DisplaySortedCountriesPlusEmployeeCount(List<Employee> employees)
        {
            Console.WriteLine("GERMANY     30000");
            Console.WriteLine("USA         50000");
            Console.WriteLine("MEXICO      20000");
        }



        /*
            Code Challenge 3
            -----------------
            There are employees from all over the world!
            The company is big and it has hundreds of thousands.
            From the subset of employees that live in a particular country,
            Display the employees with the same order as the original list that 
            lie in the positions in the range [start, end)

            ---------------
            Sample Output for start=0, end=4, country=USA
            ---------------
            Williams Mason USA 54
            Brown James USA 42
            Smith James USA 39
            Brown Mason USA 43
            Miller Mason USA 68
            */
        public void DisplayEmployeesInRangeByCountry(List<Employee> employees, int start, int end, String country)
        {
            Console.WriteLine(employees[0]);
            Console.WriteLine(employees[1]);
        }



        /*
            Code Challenge 4
            -----------------
            There are employees from all over the world!
            The company is big and it has hundreds of thousands.

            What is the meaning of life? 42

            As it turns out, some employees from the same country have
            the same last names and first names.
            We would like to remove the duplicates without regards to age,
            and have a sorted list of employees by country.
            For each country in alphabetical order, obtain the list mentioned above,
            and display the employee that lies at position 42.

            NOTE: The Employee class already knows how to compare employees
            without regard to age

            ---------------
            Sample Output
            ---------------
            Jules Vernes CANADA 51
            Jules Jacob MEXICO 62
            Jules Vernes USA 64
            */
        public void DisplaySortedCountriesWithMeaningOfLifeEmployee(List<Employee> employees)
        {
            Console.WriteLine(employees[42]);
            Console.WriteLine(employees[42]);
        }



        static void Main(string[] args)
        {
            InterviewExercises myapp = new InterviewExercises();

            List<Employee> employees = myapp.CreateEmployees();


            Console.WriteLine("Display Min age/Max age employees and Average Age");
            Console.WriteLine("------------------------------");
            myapp.DisplayMinAgeMaxAgeEmployeePlusAverage(employees);
            Console.WriteLine();


            Console.WriteLine("Countries with Employee Count");
            Console.WriteLine("------------------------------");
            myapp.DisplayCountriesPlusEmployeeCount(employees.GetRange(0, 100));
            Console.WriteLine();


            Console.WriteLine("Sorted Countries with Employee Count");
            Console.WriteLine("------------------------------");
            myapp.DisplaySortedCountriesPlusEmployeeCount(employees);
            Console.WriteLine();


            int startRange = 15;
            int endRange = 20;
            String country = "MEXICO";
            Console.WriteLine("Employees in {0} starting at position {1} up to {2}", country, startRange, endRange);
            Console.WriteLine("------------------------------");
            myapp.DisplayEmployeesInRangeByCountry(employees, startRange, endRange, country);
            Console.WriteLine();


            Console.WriteLine("Sorted Countries With Meaning-of-Life Employee");
            Console.WriteLine("------------------------------");
            myapp.DisplaySortedCountriesWithMeaningOfLifeEmployee(employees);
        }



        public List<Employee> CreateEmployees()
        {
            var firstNames = new[] {
                "Liam",
                "Noah", "Noah", "William",
                "William", "William",
                "James", "James", "James", "James",
                "Logan", "Logan", "Logan", "Logan", "Logan",
                "Benjamin", "Benjamin", "Benjamin", "Benjamin", "Benjamin",
                "Mason", "Mason", "Mason", "Mason",
                "Elijah", "Elijah", "Elijah",
                "Oliver", "Oliver",
                "Jacob"
            };
            var lastNames = new[] {
                "Smith",
                "Johnson", "Johnson",
                "Williams", "Williams", "Williams",
                "Jones", "Jones", "Jones", "Jones",
                "Brown", "Brown", "Brown", "Brown", "Brown",
                "Davis", "Davis", "Davis", "Davis", "Davis",
                "Miller",  "Miller", "Miller", "Miller",
                "Wilson", "Wilson", "Wilson",
                "Moore", "Moore",
                "Taylor"
            };
            var countries = new[] {
                "USA", "USA", "USA", "USA", "USA", "USA", "USA", "USA", "USA",
                "MEXICO", "MEXICO", "MEXICO", "MEXICO",
                "CANADA"
            };
            var employees = new List<Employee>();
            Random rand = new Random(42);
            foreach (int i in Enumerable.Range(0, 100000))
            {
                int randomNum1 = rand.Next(firstNames.Length);
                int randomNum2 = rand.Next(lastNames.Length);
                int randomNum3 = rand.Next(countries.Length);
                int randomNum4 = rand.Next(60) + 10;
                var employee = new Employee
                {
                    firstName = firstNames[randomNum1],
                    lastName = lastNames[randomNum2],
                    country = countries[randomNum3],
                    age = randomNum4
                };
                employees.Add(employee);
            }
            return employees;
        }
    }


    struct Employee: IComparable<Employee> {
        public String lastName;
        public String firstName;
        public String country;
        public int age;

        public override String ToString()
        {
            return lastName + " " + firstName + " " + country + " " + age.ToString();
        }

        public int CompareTo(Employee other)
        {
            return String.CompareOrdinal(this.lastName, other.lastName) == 0 ?
                         String.CompareOrdinal(this.firstName, other.firstName) :
                         String.CompareOrdinal(this.lastName, other.lastName); ;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var employee = (Employee)obj;
            return employee.lastName == this.lastName &&
                   employee.firstName == this.firstName &&
                   employee.country == this.country &&
                   employee.age == this.age;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                // Choose large primes to avoid hashing collisions
                const int HashingBase = (int)2166136261;
                const int HashingMultiplier = 16777619;

                int hash = HashingBase;
                hash = (hash * HashingMultiplier) ^ (!Object.ReferenceEquals(null, lastName) ? lastName.GetHashCode() : 0);
                hash = (hash * HashingMultiplier) ^ (!Object.ReferenceEquals(null, firstName) ? firstName.GetHashCode() : 0);
                hash = (hash * HashingMultiplier) ^ (!Object.ReferenceEquals(null, country) ? country.GetHashCode() : 0);
                hash = (hash * HashingMultiplier) ^ (!Object.ReferenceEquals(null, age) ? age.GetHashCode() : 0);
                return hash;
            }
        }
    }
}
