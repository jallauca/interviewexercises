//  Interview Exercises
//
//  Created by Jaime Allauca on 9/28/18.
//

import java.util.ArrayList;
import java.util.Random;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Set;
import java.util.TreeSet;
import java.lang.Math;

public class InterviewExercises {

    /* 
        ---------------
        Code Challenges
        ---------------
        For these code challenges, focus on correctness and clarity.
        Each challenge receives a list of Employee's as input and expect one particular output to be correct.

        ---------------
        Employee class
        ---------------
        class Employee implements Comparable<Employee> {
            String lastName;
            String firstName;
            String country;
            int age;
        }

        ---------------
        Sample Data
        ---------------
        lastName firstName country age
        ---------------
        Williams Mason USA 54
        Wilson Liam USA 48
        Miller Benjamin MEXICO 12
        Brown James USA 42      
        Smith James USA 39
    */



    /*
        Code Challenge 0 (Remote Interview)
        -----------------
        There are employees from all over the world!
        Display the employee with the minimum age. If more than one employee
        meets that criteria, choose the FIRST encountered.
        Display the employee with the maximum age. If more than one employee
        meets that criteria, choose the LAST encountered.
        Display the average employee age.

        ---------------
        Sample Output
        ---------------
        Min Age Employee: Jules Vernes MEXICO 10
        Min Age Employee: Albert Einstein USA 69
        Average Age: 37
    */
    public void displayMinAgeMaxAgeEmployeePlusAverage(List<Employee> employees) {
        System.out.printf("Min Age Employee: %s\n", employees.get(0));
        System.out.printf("Max Age Employee: %s\n", employees.get(1));
        System.out.printf("Average Age: %d\n", employees.get(0).age);
    }



    /*
        Code Challenge 1
        -----------------
        There are employees from all over the world!
        Display the list of countries where the employees live.
        Next to the country name, display how many employees live in that country

        ---------------
        Sample Output
        ---------------
        GERMANY     30
        USA         50
        MEXICO      20
    */
    public void displayCountriesPlusEmployeeCount(List<Employee> employees) {
        System.out.println("GERMANY     30");
        System.out.println("USA         50");
        System.out.println("MEXICO      20");
    }



    /*
        Code Challenge 2
        -----------------
        There are employees from all over the world!
        The company is big and it has hundreds of thousands.
        Display the list of countries where the employees live in alphabetical order.
        Next to the country name, display how many employees live in that country

        ---------------
        Sample Output
        ---------------
        GERMANY     30000
        MEXICO      20000
        USA         50000
    */
    public void displaySortedCountriesPlusEmployeeCount(List<Employee> employees) {
        System.out.println("GERMANY     30000");
        System.out.println("MEXICO      20000");
        System.out.println("USA         50000");
    }



    /*
        Code Challenge 3
        -----------------
        There are employees from all over the world!
        The company is big and it has hundreds of thousands.
        From the subset of employees that live in a particular country,
        Display the employees with the same order as the original list that 
        lie in the positions in the range [start, end)

        ---------------
        Sample Output for start=0, end=4, country=USA
        ---------------
        Williams Mason USA 54
        Brown James USA 42
        Smith James USA 39
        Brown Mason USA 43
        Miller Mason USA 68
    */
    public void displayEmployeesInRangeByCountry(List<Employee> employees, int start, int end, String country) {
        System.out.println(employees.get(0));
        System.out.println(employees.get(1));
    }



    /*
        Code Challenge 4
        -----------------
        There are employees from all over the world!
        The company is big and it has hundreds of thousands.

        What is the meaning of life? 42

        As it turns out, some employees from the same country have
        the same last names and first names.
        We would like to remove the duplicates without regards to age,
        and have a sorted list of employees by country.
        For each country in alphabetical order, obtain the list mentioned above,
        and display the employee that lies at position 42.

        NOTE: The Employee class already knows how to compare employees
        without regard to age

        ---------------
        Sample Output
        ---------------
        Jules Vernes CANADA 51
        Jules Jacob MEXICO 62
        Jules Vernes USA 64
    */
    public void displaySortedCountriesWithMeaningOfLifeEmployee(List<Employee> employees) {
        System.out.println(employees.get(42));
        System.out.println(employees.get(42));
    }
    
    public static void main(String[] args) {
        InterviewExercises myapp = new InterviewExercises();

        List<Employee> employees = myapp.createEmployees();


        System.out.println("Display Min age/Max age employees and Average Age");
        System.out.println("------------------------------");
        myapp.displayMinAgeMaxAgeEmployeePlusAverage(employees);
        System.out.println();


        System.out.println("Countries with Employee Count");
        System.out.println("------------------------------");
        myapp.displayCountriesPlusEmployeeCount(employees.subList(0, 100));
        System.out.println();


        System.out.println("Sorted Countries with Employee Count");
        System.out.println("------------------------------");
        myapp.displaySortedCountriesPlusEmployeeCount(employees);
        System.out.println();


        int startRange = 15;
        int endRange = 20;
        String country = "MEXICO";
        System.out.printf("Employees in %s starting at position %d up to %d\n", country, startRange, endRange);
        System.out.println("------------------------------");
        myapp.displayEmployeesInRangeByCountry(employees, startRange, endRange, country);
        System.out.println();


        System.out.println("Sorted Countries With Meaning-of-Life Employee");
        System.out.println("------------------------------");
        myapp.displaySortedCountriesWithMeaningOfLifeEmployee(employees);
    }

    public List<Employee> createEmployees() {
        String firstNames[] = {
            "Liam", 
            "Noah", "Noah", "William",
            "William", "William",
            "James", "James", "James", "James",
            "Logan", "Logan", "Logan", "Logan", "Logan",
            "Benjamin", "Benjamin", "Benjamin", "Benjamin", "Benjamin",
            "Mason", "Mason", "Mason", "Mason",
            "Elijah", "Elijah", "Elijah",
            "Oliver", "Oliver",
            "Jacob"
        };
        String lastNames[] = {
            "Smith",
            "Johnson", "Johnson",
            "Williams", "Williams", "Williams",
            "Jones", "Jones", "Jones", "Jones",
            "Brown", "Brown", "Brown", "Brown", "Brown", 
            "Davis", "Davis", "Davis", "Davis", "Davis",
            "Miller",  "Miller", "Miller", "Miller", 
            "Wilson", "Wilson", "Wilson", 
            "Moore", "Moore",
            "Taylor" 
        };
        String countries[] = {
            "USA", "USA", "USA", "USA", "USA", "USA", "USA", "USA", "USA",
            "MEXICO", "MEXICO", "MEXICO", "MEXICO",
            "CANADA"
        };
        List<Employee> employees = new ArrayList<Employee>();
        Random rand = new Random(42);
        for(int i = 0; i < 100000; i++) {
            int randomNum1 = rand.nextInt(firstNames.length);
            int randomNum2 = rand.nextInt(lastNames.length);
            int randomNum3 = rand.nextInt(countries.length);
            int randomNum4 = rand.nextInt(60) + 10;
            Employee employee = new Employee();
            employee.firstName = firstNames[randomNum1];
            employee.lastName = lastNames[randomNum2];
            employee.country = countries[randomNum3];
            employee.age = randomNum4;
            employees.add(employee);
        }
        return employees;
    }
}

class Employee implements Comparable<Employee> {
    String lastName;
    String firstName;
    String country;
    int age;

    @Override
    public String toString() {
        return lastName + " " + firstName + " " + country + " " + Integer.toString(age);
    }

    @Override
    public int compareTo(Employee other) {
        if (this.lastName.compareTo(other.lastName) == 0) {
            return this.firstName.compareTo(other.firstName);
        }
        return this.lastName.compareTo(other.lastName);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof Employee)) {
            return false;
        }

        Employee employee = (Employee)obj;
        return  employee.lastName == this.lastName &&
                employee.firstName == this.firstName &&
                employee.country == this.country &&
                employee.age == this.age;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, country, age);
    }

}