#include <vector>
#include <string>
#include <iostream>
#include <cstdlib>
#include <map>
#include <set>

using namespace std;

struct Employee {
public:
    string lastName;
    string firstName;
    string country;
    int age;

    friend ostream& operator<<(ostream& os, const Employee& e);
};

ostream& operator<<(ostream& os, const Employee& e) {
    os << e.lastName << ' ' << e.firstName << ' ' << e.country << ' ' << e.age;
    return os;
}

bool operator <(const Employee& x, const Employee& y) {
    return std::tie(x.lastName, x.firstName) < std::tie(y.lastName, y.firstName);
}

class InterviewExercises
{
public:
    /* 
        ---------------
        Code Challenges
        ---------------
        For these code challenges, focus on correctness and clarity.
        Each challenge receives a list of Employee's as input and expect one particular output to be correct.

        ---------------
        Employee class
        ---------------
        struct Employee {
        public:
            string lastName;
            string firstName;
            string country;
            int age;

             friend ostream& operator<<(ostream& os, const Employee& e);
        };

        ---------------
        Sample Data
        ---------------
        lastName firstName country age
        ---------------
        Williams Mason USA 54
        Wilson Liam USA 48
        Miller Benjamin MEXICO 12
        Brown James USA 42      
        Smith James USA 39
    */



    /*
        Code Challenge 0 (Remote Interview)
        -----------------
        There are employees from all over the world!
        Display the employee with the minimum age. If more than one employee
        meets that criteria, choose the FIRST encountered.
        Display the employee with the maximum age. If more than one employee
        meets that criteria, choose the LAST encountered.
        Display the average employee age.

        ---------------
        Sample Output
        ---------------
        Min Age Employee: Jules Vernes MEXICO 10
        Min Age Employee: Albert Einstein USA 69
        Average Age: 37
    */
    void displayMinAgeMaxAgeEmployeePlusAverage(vector<Employee> employees)
    {
        cout << "Min Age Employee: " << employees[0] << endl;
        cout << "Min Age Employee: " << employees[1] << endl;
        cout << "Average Age: " << employees[0].age << endl;
    }



    /*
        Code Challenge 1
        -----------------
        There are employees from all over the world!
        Display the list of countries where the employees live.
        Next to the country name, display how many employees live in that country

        ---------------
        Sample Output
        ---------------
        GERMANY     30
        USA         50
        MEXICO      20
        */
    void displayCountriesPlusEmployeeCount(vector<Employee> employees)
    {
        cout << "GERMANY     30" << endl;
        cout << "USA         50" << endl;
        cout << "MEXICO      20" << endl;
    }




    /*
        Code Challenge 2
        -----------------
        There are employees from all over the world!
        The company is big and it has hundreds of thousands.
        Display the list of countries where the employees live in alphabetical order.
        Next to the country name, display how many employees live in that country

        ---------------
        Sample Output
        ---------------
        GERMANY     30000
        MEXICO      20000
        USA         50000
        */
    void displaySortedCountriesPlusEmployeeCount(vector<Employee> employees)
    {
        cout << "GERMANY     30000" << endl;
        cout << "MEXICO      20000" << endl;
        cout << "USA         50000" << endl;
    }



    /*
        Code Challenge 3
        -----------------
        There are employees from all over the world!
        The company is big and it has hundreds of thousands.
        From the subset of employees that live in a particular country,
        Display the employees with the same order as the original list that 
        lie in the positions in the range [start, end)

        ---------------
        Sample Output for start=0, end=4, country=USA
        ---------------
        Williams Mason USA 54
        Brown James USA 42
        Smith James USA 39
        Brown Mason USA 43
        Miller Mason USA 68
        */
    void displayEmployeesInRangeByCountry(vector<Employee> employees, int start, int end, string country)
    {
        cout << employees[0] << endl;
        cout << employees[1] << endl;
    }



    /*
        Code Challenge 4
        -----------------
        There are employees from all over the world!
        The company is big and it has hundreds of thousands.

        What is the meaning of life? 42

        As it turns out, some employees from the same country have
        the same last names and first names.
        We would like to remove the duplicates without regards to age,
        and have a sorted list of employees by country.
        For each country in alphabetical order, obtain the list mentioned above,
        and display the employee that lies at position 42.

        NOTE: The Employee class already knows how to compare employees
        without regard to age

        ---------------
        Sample Output
        ---------------
        Jules Vernes CANADA 51
        Jules Jacob MEXICO 62
        Jules Vernes USA 64
        */
    void displaySortedCountriesWithMeaningOfLifeEmployee(vector<Employee> employees)
    {
        cout << employees[42] << endl;
        cout << employees[42] << endl;
    }



    vector<Employee> createEmployees()
    {
        string firstNames[] = {
            "Liam",
            "Noah", "Noah", "William",
            "William", "William",
            "James", "James", "James", "James",
            "Logan", "Logan", "Logan", "Logan", "Logan",
            "Benjamin", "Benjamin", "Benjamin", "Benjamin", "Benjamin",
            "Mason", "Mason", "Mason", "Mason",
            "Elijah", "Elijah", "Elijah",
            "Oliver", "Oliver",
            "Jacob"
        };
        string lastNames[] {
            "Smith",
            "Johnson", "Johnson",
            "Williams", "Williams", "Williams",
            "Jones", "Jones", "Jones", "Jones",
            "Brown", "Brown", "Brown", "Brown", "Brown",
            "Davis", "Davis", "Davis", "Davis", "Davis",
            "Miller",  "Miller", "Miller", "Miller",
            "Wilson", "Wilson", "Wilson",
            "Moore", "Moore",
            "Taylor"
        };
        string countries[] {
            "USA", "USA", "USA", "USA", "USA", "USA", "USA", "USA", "USA",
            "MEXICO", "MEXICO", "MEXICO", "MEXICO",
            "CANADA"
        };
        vector<Employee> employees;
        srand(42);
        for (int i = 0; i < 100000; i++) {
            int randomNum1 = rand()%sizeof(lastNames)/sizeof(string);
            int randomNum2 = rand()%sizeof(firstNames)/sizeof(string);
            int randomNum3 = rand()%sizeof(countries)/sizeof(string);
            int randomNum4 = rand()%60 + 10;
            Employee employee
            {
                lastNames[randomNum1],
                firstNames[randomNum2],
                countries[randomNum3],
                randomNum4
            };
            employees.push_back(employee);
        }
        return employees;
    }

};


int main()
{
    InterviewExercises myapp;

    auto employees = myapp.createEmployees();


    cout << "Display Min age/Max age employees and Average Age" << endl;
    cout << "------------------------------" << endl;
    myapp.displayMinAgeMaxAgeEmployeePlusAverage(employees);
    cout << endl;


    cout << "Countries with Employee Count" << endl;
    cout << "------------------------------" << endl;
    myapp.displayCountriesPlusEmployeeCount(vector<Employee>(employees.begin(), employees.begin() + 100));
    cout << endl;


    cout << "Sorted Countries with Employee Count" << endl;
    cout << "------------------------------" << endl;
    myapp.displaySortedCountriesPlusEmployeeCount(employees);
    cout << endl;


    int startRange = 15;
    int endRange = 20;
    string country = "MEXICO";
    printf("Employees in %s starting at position %d up to %d", country.c_str(), startRange, endRange);
    cout << "------------------------------" << endl;
    myapp.displayEmployeesInRangeByCountry(employees, startRange, endRange, country);
    cout << endl;


    cout << "Sorted Countries With Meaning-of-Life Employee" << endl;
    cout << "------------------------------" << endl;
    myapp.displaySortedCountriesWithMeaningOfLifeEmployee(employees);
}
