'use strict'

class InterviewExercises {

    /*
        Code Challenge 0 (Remote Interview)
        -----------------
        There are employees from all over the world!
        Display the employee with the minimum age. If more than one employee
        meets that criteria, choose the FIRST encountered.
        Display the employee with the maximum age. If more than one employee
        meets that criteria, choose the LAST encountered.
        Display the average employee age.

        ---------------
        Sample Output
        ---------------
        Min Age Employee: Jules Vernes MEXICO 10
        Min Age Employee: Albert Einstein USA 69
        Average Age: 37
    */
    displayMinAgeMaxAgeEmployeePlusAverage(employees) {
        console.log("Min Age Employee: ", employees[0].toString());
        console.log("Min Age Employee: ", employees[1].toString());
        console.log("Average Age: ", employees[0].age);
    }



    /*
        Code Challenge 1
        -----------------
        There are employees from all over the world!
        Display the list of countries where the employees live.
        Next to the country name, display how many employees live in that country

        ---------------
        Sample Output
        ---------------
        GERMANY     30
        USA         50
        MEXICO      20
    */
    displayCountriesPlusEmployeeCount(employees) {
        console.log("GERMANY     30");
        console.log("USA         50");
        console.log("MEXICO      20");
    }



    /*
        Code Challenge 2
        -----------------
        There are employees from all over the world!
        The company is big and it has hundreds of thousands.
        Display the list of countries where the employees live in alphabetical order.
        Next to the country name, display how many employees live in that country

        ---------------
        Sample Output
        ---------------
        GERMANY     30000
        MEXICO      20000
        USA         50000
    */
    displaySortedCountriesPlusEmployeeCount(employees) {
        console.log("GERMANY     30000");
        console.log("MEXICO      20000");
        console.log("USA         50000");
    }



    /*
        Code Challenge 3
        -----------------
        There are employees from all over the world!
        The company is big and it has hundreds of thousands.
        From the subset of employees that live in a particular country,
        Display the employees with the same order as the original list that 
        lie in the positions in the range [start, end)

        ---------------
        Sample Output for start=0, end=4, country=USA
        ---------------
        Williams Mason USA 54
        Brown James USA 42
        Smith James USA 39
        Brown Mason USA 43
        Miller Mason USA 68
    */
    displayEmployeesInRangeByCountry(employees, start, end, country) {
        console.log(`${employees[0]}`);
        console.log(`${employees[1]}`);
    }



    /*
        Code Challenge 4
        -----------------
        There are employees from all over the world!
        The company is big and it has hundreds of thousands.

        What is the meaning of life? 42

        As it turns out, some employees from the same country have
        the same last names and first names.
        We would like to remove the duplicates without regards to age,
        and have a sorted list of employees by country.
        For each country in alphabetical order, obtain the list mentioned above,
        and display the employee that lies at position 42.

        NOTE: The Employee class already knows how to compare employees
        without regard to age

        ---------------
        Sample Output
        ---------------
        Jules Vernes CANADA 51
        Jules Jacob MEXICO 62
        Jules Vernes USA 64
    */
    displaySortedCountriesWithMeaningOfLifeEmployee(employees) {
        console.log(`${employees[42]}`);
        console.log(`${employees[42]}`);
    }

    main() {
        var employees = this.createEmployees();


        console.log("Display Min age/Max age employees and Average Age");
        console.log("------------------------------");
        this.displayMinAgeMaxAgeEmployeePlusAverage(employees);
        console.log("");



        console.log("Countries with Employee Count");
        console.log("------------------------------");
        this.displayCountriesPlusEmployeeCount(employees.slice(0, 100));
        console.log("");



        console.log("Sorted Countries with Employee Count");
        console.log("------------------------------");
        this.displaySortedCountriesPlusEmployeeCount(employees);
        console.log("");



        var startRange = 15;
        var endRange = 20;
        var country = "MEXICO";
        console.log("Employees in %s starting at position %d up to %d", country, startRange, endRange);
        console.log("------------------------------");
        this.displayEmployeesInRangeByCountry(employees, startRange, endRange, country);
        console.log("");



        console.log("Sorted Countries With Meaning-of-Life Employee");
        console.log("------------------------------");
        this.displaySortedCountriesWithMeaningOfLifeEmployee(employees);
    }

    createEmployees() {
        var firstNames = [
            "Liam", 
            "Noah", "Noah", "William",
            "William", "William",
            "James", "James", "James", "James",
            "Logan", "Logan", "Logan", "Logan", "Logan",
            "Benjamin", "Benjamin", "Benjamin", "Benjamin", "Benjamin",
            "Mason", "Mason", "Mason", "Mason",
            "Elijah", "Elijah", "Elijah",
            "Oliver", "Oliver",
            "Jacob"
        ];
        var lastNames = [
            "Smith",
            "Johnson", "Johnson",
            "Williams", "Williams", "Williams",
            "Jones", "Jones", "Jones", "Jones",
            "Brown", "Brown", "Brown", "Brown", "Brown", 
            "Davis", "Davis", "Davis", "Davis", "Davis",
            "Miller",  "Miller", "Miller", "Miller", 
            "Wilson", "Wilson", "Wilson", 
            "Moore", "Moore",
            "Taylor" 
        ];
        var countries = [
            "USA", "USA", "USA", "USA", "USA", "USA", "USA", "USA", "USA",
            "MEXICO", "MEXICO", "MEXICO", "MEXICO",
            "CANADA"
        ];
        var employees = [];
        for(var i = 0; i < 100000; i++) {
            var randomNum1 = parseInt(Math.seededRandom(0, lastNames.length));
            var randomNum2 = parseInt(Math.seededRandom(0, firstNames.length));
            var randomNum3 = parseInt(Math.seededRandom(0, countries.length));
            var randomNum4 = parseInt(Math.seededRandom(0, 60) + 10);
            var employee = new Employee(
                firstNames[randomNum1],
                lastNames[randomNum2],
                countries[randomNum3],
                randomNum4
               );
            employees.push(employee);
        }
        return employees;
    }
}


Math.seed = 42;

// in order to work 'Math.seed' must NOT be undefined,
// so in any case, you HAVE to provide a Math.seed
Math.seededRandom = function(max, min) {
    max = max || 1;
    min = min || 0;

    Math.seed = (Math.seed * 9301 + 49297) % 233280;
    var rnd = Math.seed / 233280;

    return min + rnd * (max - min);
}


class Employee {

    constructor(lastName, firstName, country, age) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.country = country;
        this.age = age;
    }

    toString() {
        return this.lastName + " " + this.firstName + " " + this.country + " " + this.age;
    }

    static compare(o1, o2) {
        if (o1.lastName === o2.lastName) {
            return (o1.firstName === o2.firstName, 0) ? 0 :
                   (o1.firstName < o2.firstName) ? -1 : 1;
        }

        return (o1.lastName < o2.lastName) ? -1 : 1;
    }
}


var myapp = new InterviewExercises();
myapp.main()