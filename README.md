# Interview Exercises

## Instructions

### Java
On a terminal:
`
$ grade run
`

### Javascript
* Open as a node project. Run. Or,
* Open in a browser. Run.

### Swift
Open the XCode project. Run.

### C\#
Open the C# project. Run

### Python
On a terminal:
`
$ python InterviewExercises.py
`

### CPP
`
$ g++ -g std=c++11 InterviewExercises.cpp -o InterviewExercises
$ ./InterviewExercises
`
